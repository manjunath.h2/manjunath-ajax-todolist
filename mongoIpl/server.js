//highcharts are drawn by importing ipl project if u want reffer ipl folder on aboue

'use strict';
const express = require("express");
const app = express();
const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/ipl");
app.use(express.json());
app.use(express.static("/home/dev/projects/ipl"))
var schema = mongoose.Schema;
var todoSchema = new schema({});
let matches = mongoose.model("match", todoSchema);
let deliveries = mongoose.model("deliveries", todoSchema);
let out = [];



function getMatchPlayedPerYear(cb) {
    matches.aggregate([{
        $group: {
            _id: "$season",
            total: {
                $sum: 1
            }
        }
    }, {
        $sort: {
            _id: 1
        }
    }], function(err, result) {
        if (err) throw err;
        else {
            for (let key in result)
                out.push({
                    name: result[key]._id,
                    y: result[key].total
                })

            cb(out);
        }
    })
};

function getAllTeamsWonPerYear(cb) {
    let out = [],
        result = [];
    matches.aggregate(
        [{
            $group: {
                _id: {
                    season: "$season",
                    winner: '$winner'
                },
                count: {
                    $sum: 1
                }
            }
        }, {
            $sort: {
                _id: 1
            }

        }], (err, data) => {
            if (err) throw err
            else {
                for (let key in data)
                    out.push({
                        season: data[key]._id.season,
                        winner: data[key]._id.winner,
                        count: data[key].count
                    })
                let count = 0,
                    counterArray = [];
                getTeamData(team => {
                    getYearData(year => {

                    });
                    cb(out);
                });

            }
        }
    )
};

function getExtraRunsConceededPerTeam(cb) {
    let out = [];
    matches.aggregate(
        [{
            $match: {
                season: 2016
            }
        }, {
            $lookup: {
                from: "deliveries",
                localField: "id",
                foreignField: "match_id",
                as: "delivery"
            }
        }, {
            $unwind: "$delivery"
        }, {
            $group: {
                _id: "$delivery.bowling_team",
                count: {
                    $sum: "$delivery.extra_runs"
                }
            }
        }, {
            $sort: {
                _id: 1
            }
        }], (err, data) => {
            if (err) throw err;
            else {
                for (let key in data)
                    out.push({
                        name: data[key]._id,
                        y: data[key].count
                    });

                cb(out);
            }
        })
}

function getTeamData(cb) {
    let out = [];
    matches.aggregate(
        [{
            $group: {
                _id: "$team1"
            }
        }, {
            $sort: {
                _id: 1
            }
        }], (err, data) => {
            if (err) throw err
            else {
                for (let key in data) {
                    out.push(data[key]._id);
                }
                cb(out)
            }
        })
}

function getYearData(cb) {
    let out = [];
    matches.aggregate(
        [{
            $group: {
                _id: "$season"
            }
        }, {
            $sort: {
                _id: 1
            }
        }], (err, data) => {
            if (err) throw err
            else {
                for (let key in data) {
                    out.push(data[key]._id);
                }
                cb(out)
            }
        })
}

function getDel(cb) {
    let out = [];
    deliveries.find(data => {
        for (let key in data) {
            out.push(data);
        }
        cb(out);
    });
}
app.get("/get", (req, res) => {
    getDel(data => {
        res.send(data);
    })
})
app.get("/getData/matches/played/per/season", (req, res) => {
    getMatchPlayedPerYear(data => {
        res.send(data);
    })
});
app.get("/getData/matches/wonBy/allTeams/perYear", (req, res) => {
    getAllTeamsWonPerYear(data => {
        res.send(data);
    })
});
app.get("/getData/ofThe/year2016/extra/runs/conceded/perTeam", (req, res) => {
    getExtraRunsConceededPerTeam(data => {
        res.send(data);
    });
});
app.get("/getData/matches/team", (req, res) => {
    getTeamData(data => {
        res.send(data);
    });
});
app.get("/getData/matches/year", (req, res) => {
    getYearData(data => {
        res.send(data);
    });
})
app.listen(3000);


//using stages but need more time if u want add it aboue so that  u can get extra runs conceeded per team in a specific year

// deliveries.aggregate(
//     [{
//         $lookup: {
//             from: "matches",
//             localField: "match_id",
//             foreignField: "id",
//             as: "matches"
//         }
//     }, {
//         $project: {
//             bowling_team: 1,
//             extra_runs: 1,
//             matches: {
//                 season: 1
//             }
//         }
//     }, {
//         $match: {
//             matches: {
//                 season: 2016
//             }
//         }
//     }, {
//         $project: {
//             bowling_team: 1,
//             extra_runs: 1
//         }
//     }, {
//         $group: {
//             _id: "$bowling_team",
//             count: {
//                 $sum: "$extra_runs"
//             }
//         }
//     }, {
//         $sort: {
//             _id: 1
//         }
//     }]

//this is using replaceroot which need more time but less line number u can get extra runs conceeded per team in a specific year
// db.deliveries.aggregate([{
//     $lookup: {
//         from: "deliveries",
//         localField: "match_id",
//         foreignField: "id",
//         as: "matches"
//     }
// }, {
//     $replaceRoot: {
//         newRoot: {
//             $mergeObjects: [{
//                 $arrayElemAt: ["$matches", 0]
//             }, "$$ROOT"]
//         }
//     }
// }, {
//     $project: {
//         matches: 0
//     }
// },{
//     $match:{season:2016}
// },{
//     $group:{_id:"$bowling_team",
// count:{$sum:"$extra_runs"}}
// }]).pretty()