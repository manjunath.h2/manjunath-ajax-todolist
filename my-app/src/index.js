import ReactDOM from 'react-dom';
import './index.css';
import React,{Component} from 'react';
import Year from './year';

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }
  componentDidMount() {
    this.timer = setInterval(()=>this.tick(),1000);
  }
  
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}
  
  
  // ========================================
  
  ReactDOM.render(
    <Clock />,
    document.getElementById('root')
  );
