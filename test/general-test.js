const should = require("chai").should();
const functions = require("/home/dev/projects/ipl/general.js");
let dataOfMatches = require("./matchDataSample");
let dataOfDelieveries = require("./deliveriesDataSample");
let dataOfObject = { key: "kji" };
let dataOfNothing = "";
var obtainedoutput = 0;

describe("these functions performs their respective task as given below", () => {

    it("getIdsOfNeededRecords should give the id's of needed record of respective year passed as a string", () => {
        obtainedoutput = functions.getIdOfNeededRecords(dataOfMatches, "2015");
        expectedOutput = ["7", "8", "9", "10", "12"];
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getIdsOfNeededRecords should give the eror if i pass the empty data to anyone of the arguement ", () => {
        obtainedoutput = functions.getIdOfNeededRecords(dataOfNothing, "2015");
        expectedOutput = "arguement error";
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getyearData Should give only the years in the record uniquely", () => {
        obtainedoutput = functions.getyearData(dataOfMatches);
        expectedOutput = ["2015", "2016", "2014"];
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getYearData should give the eror if i pass the empty data to the arguement", () => {
        obtainedoutput = functions.getyearData(dataOfNothing);
        expectedOutput = "arguement error";
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getTeamData Should give only the Team who win the each matches", () => {
        obtainedoutput = functions.getTeamData(dataOfMatches)
        expectedOutput = ["Mumbai Indians", "Kings XI Punjab", "Delhi Daredevils", "Kolkata Knight Riders"];
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getTeamData should give the eror if i pass the empty data to the arguement", () => {
        obtainedoutput = functions.getTeamData(dataOfNothing)
        expectedOutput = "arguement error";
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getRecordsById should give the records of needed ids of respective array of ids passed", () => {
        let id = ["1"];
        obtainedoutput = functions.getRecordsById(dataOfDelieveries, id);
        expectedOutput = [{
            "match_id": "1",
            "inning": "1",
            "batting_team": "Sunrisers Hyderabad",
            "bowling_team": "Royal Challengers Bangalore",
            "over": "1",
            "ball": "2",
            "batsman": "DA Warner",
            "non_striker": "S Dhawan",
            "bowler": "TS Mills",
            "is_super_over": "0",
            "wide_runs": "0",
            "bye_runs": "0",
            "legbye_runs": "0",
            "noball_runs": "0",
            "penalty_runs": "0",
            "batsman_runs": "0",
            "extra_runs": "0",
            "total_runs": "0",
            "player_dismissed": "",
            "dismissal_kind": "",
            "fielder": ""
        }, {
            "match_id": "1",
            "inning": "1",
            "batting_team": "Sunrisers Hyderabad",
            "bowling_team": "Royal Challengers Bangalore",
            "over": "1",
            "ball": "3",
            "batsman": "DA Warner",
            "non_striker": "S Dhawan",
            "bowler": "TS Mills",
            "is_super_over": "0",
            "wide_runs": "0",
            "bye_runs": "0",
            "legbye_runs": "0",
            "noball_runs": "0",
            "penalty_runs": "0",
            "batsman_runs": "4",
            "extra_runs": "0",
            "total_runs": "4",
            "player_dismissed": "",
            "dismissal_kind": "",
            "fielder": ""
        }, {
            "match_id": "1",
            "inning": "1",
            "batting_team": "Sunrisers Hyderabad",
            "bowling_team": "Royal Challengers Bangalore",
            "over": "1",
            "ball": "4",
            "batsman": "DA Warner",
            "non_striker": "S Dhawan",
            "bowler": "TS Mills",
            "is_super_over": "0",
            "wide_runs": "0",
            "bye_runs": "0",
            "legbye_runs": "0",
            "noball_runs": "0",
            "penalty_runs": "0",
            "batsman_runs": "0",
            "extra_runs": "0",
            "total_runs": "0",
            "player_dismissed": "",
            "dismissal_kind": "",
            "fielder": ""
        }, {
            "match_id": "1",
            "inning": "1",
            "batting_team": "Sunrisers Hyderabad",
            "bowling_team": "Royal Challengers Bangalore",
            "over": "1",
            "ball": "5",
            "batsman": "DA Warner",
            "non_striker": "S Dhawan",
            "bowler": "TS Mills",
            "is_super_over": "0",
            "wide_runs": "2",
            "bye_runs": "0",
            "legbye_runs": "0",
            "noball_runs": "0",
            "penalty_runs": "0",
            "batsman_runs": "0",
            "extra_runs": "2",
            "total_runs": "2",
            "player_dismissed": "",
            "dismissal_kind": "",
            "fielder": ""
        }];
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getRecordsById should give the error if i pass some bad arguements", () => {
        let id = ["1"];
        obtainedoutput = functions.getRecordsById(dataOfNothing, id)
        expectedOutput = "arguement error";
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getBowlersName Should give only the unique bowlers name list in an array", () => {
        obtainedoutput = functions.getBowlersName(dataOfDelieveries);
        expectedOutput = ["TS Mills"];
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });
    it("getBowlersName should give the eror if i pass the empty data to the arguement", () => {
        obtainedoutput = functions.getBowlersName(dataOfNothing)
        expectedOutput = "arguement error";
        expectedOutput.should.be.deep.equal(obtainedoutput);
    });

});