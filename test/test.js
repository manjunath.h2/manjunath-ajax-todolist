var chai = require("chai").should();
var api4 = require('/home/dev/projects/ipl/api-functions/top-economic-bowler.js');
var deliveriesData = require("./sampleDeliveryData");
var matchdata = [{ season: "2015", id: "561" }, { season: "2015", id: "565" }, { season: "2015", id: "571" }];
let dataOfNothing = "";

describe('For the year 2015 give the top 10 economical bowlers', () => {
    it("getTopEconomicBowlers should give the argument error if user pass the empty data or otherthan array in both argument", () => {
        obtainedOutput = api4(dataOfNothing, deliveriesData);
        expectedOutput = "argument error";
        expectedOutput.should.be.deep.equal(obtainedOutput);

    });

});
it("getTopEconomicBowlers should give the top ten economic bowlers name and economy of them", () => {
    obtainedOutput = api4(matchdata, deliveriesData);
    expectedOutput = [{ name: "J Yadav", y: 6.923076923076923 }];
    expectedOutput.should.be.deep.equal(obtainedOutput);
});