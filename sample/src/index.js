
import ReactDom from 'react-dom';
import React,{Component} from 'react';
import Next from './year';

class Years extends Component
{
    constructor(props)
    {
        super(props);
        this.state={toggle:true};
        this.tog=this.tog.bind(this);
    }
    tog()
    {
        this.setState({toggle:!this.state.toggle});
    }
   render()
   {
       return(
           <div>
               <button onClick={this.tog}>{this.state.toggle ? "on":"off"}</button>
               <Next  data={this.state.toggle} togParent={this.tog}/>
           </div>
       )
   } 
}
ReactDom.render(<Years/>,document.getElementById("root"));