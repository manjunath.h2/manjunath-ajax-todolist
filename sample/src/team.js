import React,{Component} from 'react';
import Charts from './charts';
class Teams extends Component
{
    constructor(props)
    {
        super(props);
        var teams=this.props.teams;
        this.teamlist=teams.map(((team,i)=>{
            return <li id={i}><Charts teams={team} charts={this.props.charts}/></li>
        }));
        this.state={
            showTeams:false
        };
        this.props.togParent=this.props.togParent.bind(this);
    }
    render()
    {
        var teams= this.teamlist;
        if(!this.state.showTeams)
        teams=null;
        return(
            <div>
                <h1 onClick={this.props.togParent}>{this.props.years}</h1>
            <ul>
                {teams}
            </ul>
            </div>
        );
    }
}

export default Teams