import React,{Component} from 'react';
class Next extends Component
{
    
        constructor(props)
        {
            super(props);
            this.state={toggle:true};
            this.handle=this.handle.bind(this);
        }
        handle(){
            this.props.togParent();
            this.setState({toggle:!this.state.toggle});
        }
       render()
       {
           let b=this.props.data;
           return(
               <button onClick={this.handle.bind}>{this.state.toggle ? "off":"on"}</button>
           )
       } 
    
}

export default Next;