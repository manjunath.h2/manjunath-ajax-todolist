import React,{Component} from 'react';
class Chart extends Component
{
    constructor(props)
    {
        super(props);
        var charts=this.props.charts;
        this.chartlist=charts.map(((chart,i)=>{
            return <li key={i}>{chart}</li>
        }));
        this.state={
            showCharts:false
        };
        this.stateHandler=this.stateHandler.bind(this);
    }
    stateHandler(){
        this.setState(prevState=>({
            showCharts:!prevState.showCharts
        }));
    }
    render()
    {
        var charts=this.chartlist;
        if(!this.state.showCharts)
        {
            charts=null;
        }
        return(
            <div>
                <h1 onClick={this.stateHandler}>{this.props.teams}</h1>
            <ul>
               {charts}
            </ul>
            </div>
        );
    }
}

export default Chart