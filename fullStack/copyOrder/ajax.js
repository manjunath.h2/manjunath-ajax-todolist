var $orders = $('.orders-list');
var $name = $('.cofee-name');
var $deliverTo = $('.plce-to-deliver');
$.ajax({
    type: 'GET',
    url: '/orders',
    success: function(orders) {
        console.log("success", orders);
        $.each(orders, function(i, order) {
            $orders.append('<li>name : ' + order.name + "," + 'deliver to : ' + order.deliverTo + '</li>');
        });
    },
    error: function() {
        alert("something went wrong");
    }

});
$('button').on('click', () => {
    $.ajax({
        type: 'POST',
        url: '/orders',
        data: {
            name: $name.val(),
            deliverTo: $deliverTo.val()
        },
        datatype: "application/json",
        success: function(newOrder) {
            $orders.append('<li>name : ' + newOrder.name + "," + 'deliver to : ' + newOrder.deliverTo + '</li>');
        },
        error: function() {
            alert("something went wrong");
        }
    });
});