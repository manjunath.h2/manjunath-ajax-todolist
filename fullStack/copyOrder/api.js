const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bp = require("body-parser");
const schema = mongoose.Schema;
var cofeeList = new schema({
    name: { type: String },
    deliverTo: { type: String }
});
const lst = mongoose.model("clists", cofeeList);
mongoose.connect("mongodb://localhost:27017/cofee");

app.use(express.static(__dirname));
app.use(bp.urlencoded({ extended: false }))

app.get("/orders", (req, res) => {
    lst.find({}, (err, data) => {
        if (err) throw err;
        else {
            res.json(data);
        }
    });
});
app.post("/orders", (req, res) => {
    var data = {
        name: req.body.name,
        deliverTo: req.body.deliverTo
    };
    var obj = new lst(data);
    obj.save((err, result) => {
        if (err) throw err;
        else
            res.json(result);
    });
});
app.delete("/orders/:id", (req, res) => {

});
app.listen(3000);