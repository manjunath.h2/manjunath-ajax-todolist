const newTask = $(".input-text input");
$(function() {
    $.ajax({
        type: 'GET',
        url: '/todos',
        success: function(tasks) {
            $.each(tasks, function(i, task) {
                createElement(task.task, task.status, task._id);
            });
        },
        error: function() {
            alert("something went wrong");
        }
    });
    $("input[type=text]").keypress(function() {
        if (event.which === 13) {
            if ($(this).val() === "") {
                $(this).attr("placeholder", "Enter a task to be done");
                $(this).css("background", "red");
            } else {
                $(this).attr("placeholder", "Add more Task");
                $(this).css("background", "white");
                var text = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: '/todos',
                    data: {
                        task: newTask.val()
                    },
                    datatype: "application/json",
                    success: function(newTasking) {
                        var text = newTasking.task;
                        createElement(text, status, newTasking._id);
                    },
                    error: function() {
                        alert("something went wrong");
                    }
                });
                $(this).val("");
            }
        }
    });
    $("ul").on("click", ".check", function() {
        if ($(this)[0].checked == true)
            $($(this).siblings()[0]).css("text-decoration", "line-through");
        else
            $($(this).siblings()[0]).css("text-decoration", "none");
        var currentId = $(this).parent()[0].id;
        $.ajax({
            type: 'PUT',
            url: '/todos/' + currentId,
            data: {
                task: $($(this).siblings()[0]).text(),
                status: $(this)[0].checked
            }
        });
    });
    $("ul").on("click", ".cancelButton", function() {
        var currentId = $(this).parent()[0].id;
        $($(this).parent()[0]).remove();
        $.ajax({
            type: 'DELETE',
            url: '/todos/' + currentId,
            data: {
                id: currentId
            }
        });
    });
    $("ul").on("click", ".clickOnTask", () => {
        $(".clickOnTask").keypress(function() {
            if (event.which === 13) {
                var currentId = $(this).parent()[0].id;
                var text = $(this).text();
                $.ajax({
                    type: 'PUT',
                    url: '/todos/' + currentId,
                    data: {
                        task: text,
                        status: $(this).siblings()[0].checked
                    }
                });
            }
        });
    });
});

function createElement(text, status, id) {
    if (status === true) {
        checked = "checked";
        scratch = "text-decoration:line-through";
    } else {
        checked = "";
        scratch = "";
    }
    var listTag = ("<li class=\"list-group-items row card row-sm\" id=" + id + ">" +
        ("<input class=\"check col-1\" type=\"checkbox\" " + checked + ">") +
        ("<div class=\"col-10 clickOnTask\" contenteditable=\"true\" style=" + scratch + ">" + text + "</div>") +
        ("<button class=\"btn-light col-1 cancelButton\">&#9747;</button>") +
        "</li>");
    $(listTag).prependTo("ul");
};