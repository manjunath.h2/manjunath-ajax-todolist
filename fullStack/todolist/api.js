const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const Schema = mongoose.Schema;
const toDoList = new Schema({
    task: String,
    status: { type: Boolean, default: false }
});
const List = mongoose.model("tasks", toDoList);
mongoose.connect("mongodb://localhost:27017/list");

app.use(express.static(__dirname));
app.use(bodyParser.urlencoded({ extended: false }))

app.get("/todos", (req, res) => {
    List.find({}, (err, data) => {
        if (err) throw err;
        else {
            res.json(data);
        }
    });
});
app.post("/todos", (req, res) => {
    var data = {
        task: req.body.task,
        status: req.body.status
    };
    var object = new List(data);
    object.save((err, result) => {
        if (err) throw err;
        else
            res.json(result);
    });
});
app.delete("/todos/:id", (req, res) => {
    List.remove({ _id: req.params.id }, (err, result) => {
        if (err) throw err;
        res.send("deleted");
    });
});
app.put("/todos/:id", (req, res) => {
    List.update({ _id: req.params.id }, { task: req.body.task, status: req.body.status }, (err, result) => {
        if (err) throw err;
    });
});
app.listen(4000);