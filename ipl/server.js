//Dependencies
const express = require("express");
const app = express();
const fs = require("fs");
const csvjson = require("csvjson");
const api1 = require("./api-functions/matches-played-per-year.js");
const api2 = require("./api-functions/matches-won-by-team-per-year.js");
const api3 = require("/home/dev/projects/ipl/api-functions/top-economic-bowler.js");
const api4 = require("/home/dev/projects/ipl/api-functions/top-economic-bowler.js");
const util = require("/home/dev/projects/ipl/util.js");
let options = {
    delimiter: ',',
    quote: '"'
};

let matchData = fs.readFileSync("data/matches.csv", 'utf8');
matchData = csvjson.toObject(matchData, options);
let deliveriesData = fs.readFileSync("data/deliveries.csv", 'utf8');
deliveriesData = csvjson.toObject(deliveriesData, options);
app.use(express.static(__dirname));

function takeDataOfVirat(req, res, data1, data2) {
    let id = util.getIdOfNeededRecords(data2, "2015");
    let records = util.getRecordsById(data1, id);
    console.log("hi" + id);
    let result = [];
    records.forEach(element => {
        if (element.bowler == "J Yadav") {
            result.push(element);
        }
    });
    console.log(result);
    res.send(result);
}

//routes
app.get('/get-match-per-year', (req, res) => {
    res.send(api1(matchData));
});
app.get('/getData/matches/wonBy/allTeams/perYear', (req, res) => {
    res.send(api2(matchData));
});
app.get('/getData/ofThe/year2016/extra/runs/conceded/perTeam', (req, res) => {
    res.send(api3(matchData, deliveriesData));
});
app.get('/getdata/ofThe/topTen/economic/bowlersName', (req, res) => {
    res.send(api4(matchData, deliveriesData));
});
app.get('/getData/deliveries/virat', (req, res) => {
    takeDataOfVirat(req, res, deliveriesData, matchData);
});
app.listen(3000, () => console.log('Example app listening on port 3000!'))

// function takeData(req, res, file) {
//     fs.readFile(file, 'utf8', (err, data) => {
//         if (err) throw err;
//         res.send(JSON.parse(data));
//     });
// }

// function takeDataById(req, res, id, file) {
//     fs.readFile(file, 'utf8', (err, data) => {
//         if (err) throw err;
//         let dataa = JSON.parse(data);
//         res.send(dataa[id]);
//     });
// };




// app.get('/getData/matches', (req, res) => {
//     takeData(req, res, "data/by-csvjson-matches.json");
// });


// app.get('/getData/deliveries', (req, res) => {
//     takeData(req, res, "data/by-csvjson-deliveries.json");
// });
// app.get('/getData/deliveries/:id', (req, res) => {
//     takeDataById(req, res, req.params.id, "data/by-csvjson-deliveries.json");
// });
// app.get('/getData/deliveries/:id', (req, res) => {
//     takeDataById(req, res, req.params.id, "data/by-csvjson-deliveries.json");
// });