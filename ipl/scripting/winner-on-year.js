$('.button2').click(() => {
    $.ajax({
        type: 'GET',
        url: '/getData/matches/wonBy/allTeams/perYear',
        success: function(data) {

            Highcharts.chart('container', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Match Won By All Teams Per Year'
                },
                xAxis: {
                    categories: [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Match played on year'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: data
            });
        },
        error: function() {
            alert("something went wrong");
        }
    });
});