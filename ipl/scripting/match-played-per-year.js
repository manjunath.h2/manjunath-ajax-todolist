$('.button1').click(() => {
    $.ajax({
        type: 'GET',
        url: '/getData/matches/played/per/season',
        success: function(seasons) {
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'The number of matches played per year of all the years in IPL'
                },
                xAxis: {
                    type: 'category',
                    title: {
                        text: 'Years'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Number of matches'
                    }
                },
                legend: {
                    enabled: true
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                },

                "series": [{
                    "name": "IPL Matches",
                    "colorByPoint": true,
                    "data": seasons
                }],
            });
        },
        error: function() {
            alert("something went wrong");
        }
    });
});
$('.button5').click(() => {
    alert("coming soon.............");
});