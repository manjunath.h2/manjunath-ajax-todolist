$('.button4').click(() => {
    $.ajax({
        type: 'GET',
        url: 'getdata/ofThe/topTen/economic/bowlersName',
        success: function(data) {
            console.log(data);
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'The extra runs conceeded in the year 2016'
                },
                xAxis: {
                    type: 'category',
                    title: {
                        text: 'Years'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Number of runs'
                    }
                },
                legend: {
                    enabled: true
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.2f}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                },

                "series": [{
                    "name": "IPL Matches",
                    "colorByPoint": true,
                    "data": data
                }],
            });
        },
        error: () => {
            alert("something went wrong");
        }
    });

});