//this section consists of getIdOfNeededRecords,getyearData,getTeamData,getRecordsById functions 
//and exporting these to a module with the name of their function call name only 
function getIdOfNeededRecords(data, year) {
    if (data == "" || year == "" || typeof(year) != "string") {
        return "arguement error";
    } else {
        let dataOfId = [];
        data.forEach(element => {
            if (element.season == year) {
                dataOfId.push(element.id);
            }
        });
        return dataOfId;
    }
}

function getyearData(data) {
    if (data == "") {
        return "arguement error";
    } else {
        let yearData = [];
        data.reduce((year,currentRecord) => {
            if (currentRecord.season in year)
                year.push(currentRecord.season)
        },[]);
        return yearData;
    }

}

function getTeamData(data) {
    let teamData = [];

    if (data == "") {
        return "arguement error";
    } else {
        data.forEach(team => {
            if (teamData.indexOf(team.winner) == -1 && team.winner != "")
                teamData.push(team.winner)
        });
        teamData.sort();
        return teamData;
    }
    // let teamData = new Set(data.map(element => { return element.winner; }));
    // teamData = Array.from(teamData); //every data should be given

}

function getRecordsById(data, idsOfRecords) {
    let result = [];
    // data.forEach(element => {
    //     idsOfRecords.forEach(id => {

    //         if (element.match_id == id)
    //             result.push(element);
    //     });
    // });
    if (data == "" || idsOfRecords == "")
        return "arguement error";
    else {
        result = data.filter(record => {
            return (parseInt(record.match_id) >= idsOfRecords[0] && parseInt(record.match_id) <= idsOfRecords[idsOfRecords.length - 1]);
        });
        return result;
    }
}

function getBowlersName(data) {
    if (data == "")
        return "arguement error";
    else {
        let result = new Set(data.map(element => { return element.bowler; }));
        result = Array.from(result);
        return result;
    }
}
module.exports = ({
    getIdOfNeededRecords: getIdOfNeededRecords,
    getyearData: getyearData,
    getTeamData: getTeamData,
    getRecordsById: getRecordsById,
    getBowlersName: getBowlersName
});