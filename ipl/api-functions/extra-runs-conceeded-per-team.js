const general = require("../util");

function getDataOfTheYear2016ExtraRunsConcededPerTeam(matchData, deliveriesData) {
    let result = {};
    let output = [];
    let team = general.getTeamData(matchData);
    let idsOfRecords = general.getIdOfNeededRecords(matchData, "2016");
    let records = general.getRecordsById(deliveriesData, idsOfRecords);

    team.forEach(element => {
        records.forEach(rec => {
            if (element == rec.bowling_team) {
                if (element in result)
                    result[element] += parseInt(rec.extra_runs);
                else
                    result[element] = parseInt(rec.extra_runs);
            }
        });
    });
    for (let key in result)
        output.push({ name: key, y: result[key] });
    return output;

}
module.exports = getDataOfTheYear2016ExtraRunsConcededPerTeam;