const jeneral = require("../util")

function getDataOfMatchesWonByAllTeamsPerYear(data) {
    let year = jeneral.getyearData(data);
    let team = jeneral.getTeamData(data);
    let count = 0;
    let result = [];
    let counterArray = [];
    team.forEach(teamName => {
        year.forEach(yearName => {
            data.forEach(element => {
                if (element.season == yearName && element.winner == teamName)
                    count++;
            });
            counterArray.push(count);
            count = 0;
        });
        result.push({ name: teamName, data: counterArray });
        counterArray = [];
    });
    return result;
}

module.exports = getDataOfMatchesWonByAllTeamsPerYear;