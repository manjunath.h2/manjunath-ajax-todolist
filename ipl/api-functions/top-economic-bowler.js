const util = require("../util")

function getTopEconomicBowlers(matchData, deliverieData) {

    if (matchData == "" || deliverieData == "" || Array.isArray(matchData) == false ||
        Array.isArray(deliverieData == "false")) {
        return error;
    } 
    
    let id = util.getIdOfNeededRecords(matchData, "2015");

    let records = util.getRecordsById(deliverieData, id);
    let bowlers = util.getBowlersName(records);
    let result = [];
    bowlers.forEach(bowler => {
        let total = 0,
            count = 0;
        records.forEach(record => {
            if (record.bowler === bowler) {
                total += record.total_runs - record.legbye_runs - record.bye_runs;
                if (!(parseInt(record.wide_runs) > 0 || parseInt(record.noball_runs) > 0))
                    count++;
            }
        });
        if (count > 12)
            result.push({ name: bowler, y: total / (count / 6) });

    });
    
    result.sort(function(a, b) {
        return a.y - b.y;
    });
    let i = 0;
    let out = result.filter(record => i++ < 10);
    return out;

}

module.exports = getTopEconomicBowlers