function getDataOfMatchesPlayedPerYear(data) {
    let result = {}; //if u use this as an array uncomment the comment tag
    let final = [];
    data.forEach(element => {
        if (element.season in result) {
            result[element.season] += 1;
        } else {
            result[element.season] = 1;
        }
    });
    // for (var i = 0; i < result.length; i++) {
    //     if (result[i] != null)
    //         final.push({ name: i, y: result[i] });
    // }
    for (let key in result) {
        final.push({
            name: key,
            y: result[key]
        });
    }
    return final;
}

module.exports = getDataOfMatchesPlayedPerYear;