const express = require("express");
const app = express();
const fs = require("fs");

function takeData(req, res) {
    fs.readFile("data/by-csvjson.json", 'utf8', (err, data) => {
        if (err) throw err;
        res.send(JSON.parse(data));
    });
}

function takeDataById(req, res, id) {
    fs.readFile("data/by-csvjson.json", 'utf8', (err, data) => {
        if (err) throw err;
        var dataa = JSON.parse(data);
        res.send(dataa[id]);
    });
}

function takeDataByAttribute(req, res, attribute) {
    fs.readFile("data/by-csvjson.json", 'utf8', (err, data) => {
        if (err) throw err;
        var dataa = JSON.parse(data);
        var requiredData = [];
        for (var i = 0; i < dataa.length; i++)
            requiredData.push(dataa[i][attribute]);
        res.send(requiredData);
        // for (var i = 0; i < dataa.length; i++)
        //     res.write(JSON.stringify(dataa[i][attribute]));
        // res.end();

    });
}
app.get('/', (req, res) => {
    res.send("welcome");

});
app.get('/getdata', (req, res) => {
    takeData(req, res);
});

app.get('/getdata/:id', (req, res) => {
    takeDataById(req, res, req.params.id);
});
app.get('/getdata/attributes/:attr', (req, res) => {
    takeDataByAttribute(req, res, req.params.attr);
});

app.listen(5000);