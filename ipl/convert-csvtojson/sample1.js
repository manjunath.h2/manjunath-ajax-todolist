var fs = require("fs");
fs.readFile("data/matches.csv", 'utf8', (err, data) => {
    if (err) throw err
    var text = toJson(data);
    fs.appendFile("data/matches-converted-to-json.json", JSON.stringify(text) + "\n", 'utf8', (err) => {
        if (err) throw err;
    })
});

function toJson(csvData) {
    var lines = csvData.split("\n");
    var colNames = lines[0].split(",");
    var records = [];
    for (var i = 1; i < lines.length - 1; i++) {
        var record = {};
        var bits = lines[i].split(",");
        for (var j = 0; j < bits.length; j++) {
            record[colNames[j]] = bits[j];
        }
        records.push(record);
    }
    return records;
}