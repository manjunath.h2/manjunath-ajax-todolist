var fs = require("fs");
var csv = require("fast-csv");
var arr = [];
fs.createReadStream('data/matches.csv')
    .pipe(csv())
    .on('data', (data) => {
        arr.push(data);
        fs.appendFile("./data/converted-by-fast-csv.txt", JSON.stringify(arr), 'utf8', (err) => {
            if (err) throw err;
        });
    });