function toJson(csvData) {
    var lines = csvData.split("\n");
    var colNames = lines[0].split(",");
    var records = [];
    for (var i = 1; i < lines.length; i++) {
        var record = {};
        var bits = lines[i].split(",");
        for (var j = 0; j < bits.length; j++) {
            record[colNames[j]] = bits[j];
        }
        records.push(record);
    }
    return records;
}
var result;
var rawFile = new XMLHttpRequest();
rawFile.open("GET", "data/matches.csv", true);
rawFile.onreadystatechange = function() {
    if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status == 0) {
            var allText = rawFile.responseText;
            result = toJson(allText);
            console.log(JSON.stringify(result));
        }
    }
}
rawFile.send(null);