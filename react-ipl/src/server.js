'use strict';
const express=require("express");
const app=express();
const mongoose=require("mongoose");
mongoose.connect("mongodb://localhost:27017/ipl");
const scheema=mongoose.Schema;
const iplSchema=new scheema({});
const matches=mongoose.model("matches",iplSchema);
const deliveries=mongoose.model("deliveries",iplSchema);

app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","http://localhost:3000/");
});

function neededData(cb){
    matches.aggregate(
        [
            {
                $group:{
                    _id:"$season",
                    teams:{$addToSet:"$team1"}
                }
            },{
                $sort:{
                    _id:1
                }
            }
        ]
    )
    .then(data=>{
        cb(data);
    })
    .catch(err=>{
        cb("something went wrong");
    })
}
app.get("/",(req,res)=>{
    res.send("hi");
});
app.get("/get-needed-data",(req,res)=>{
    neededData(data=>{
        res.send(data);
    })
});
app.listen(5000);


