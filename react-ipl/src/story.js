
import React,{Component} from 'react';
class Story extends Component {
  ShowStoryHandler(){
   alert("finished");
  }
    render() {
      let story=this.props.story;
      return (
         <ul>
           {story.map(record=><li onClick={this.ShowStoryHandler}>{record}</li>)}
         </ul>
      );
    }
  }
  export default Story;