import React,{Component} from 'react'
import Story from './story';
class Teams extends Component {
  constructor(props)
  {
    super(props)
    this.state={open:false};
    this.ShowTeamsHandler=this.ShowTeamsHandler.bind(this);
  }
  ShowTeamsHandler(){
    debugger;
    this.setState(prevState=>({
      open:!prevState.open
    }));
  }
    render() {
      let teams=this.props.teams;
      return (
      <ul>
        {teams.map(record=>(<div><li onClick={this.ShowTeamsHandler}>{record}
        </li>
        {this.state.open ? <Story story={this.props.story}/>:null}
        </div>))}}
      </ul>
        
      )
    }
  }

  export default Teams;