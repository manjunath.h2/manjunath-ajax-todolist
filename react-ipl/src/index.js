import './index.css';
import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import Year from './year';
const years=[2008,2009,2010,2011,2012];
const teams=['kkr','rcb','csk'];
const story=['x','y'];

class Toggle extends Component {
    constructor(props) {
      super(props);
      this.state = ({open:false});
      this.handleClick=this.handleClick.bind(this);
    }
    handleClick() {
      this.setState(prevState => ({open:!prevState.open}));
    }
    render() {
      return (
        <div className="home">
          <button className="button" onClick={this.handleClick}>Home</button>
          {this.state.open ? <Year year={this.props.year} teams={this.props.teams} story={this.props.story}/>
            :null}
          </div>
      );
    }
  }
  
  ReactDOM.render(
    <Toggle year={years} teams={teams} story={story}/>,
    document.getElementById('root')
  );