
import React,{Component} from 'react'
import Teams from './team';
class Year extends Component {
    constructor(props)
    {
        super(props);
        this.array=this.props.year.map(each=>false);
        this.state={open:this.array};
        
    }
    ShowTeamsHandler(e){
        let id=e.target.id;
        let closeArray=this.props.year.map((each,index)=>{
            if(id!=index)
            return true;
            return false;
        });
       this.setState(prevState=>({
           open:closeArray,
       }));
    }
    render() {
        let year=this.props.year;
      return (
          <ul>
          {year.map((record,index)=>(<div key={index}><li onClick={this.ShowTeamsHandler.bind(this)} id={index}>{record}
        </li>
        {this.state.open ? <Teams teams={this.props.teams} story={this.props.story}/>:null}
        </div>))}

          </ul>
      );
    }
  }
  export default Year;