import React,{Component} from 'react';
import Teams from './teams';
class Years extends Component
{
    constructor()
    {
        super();
        this.state={show:false,show1:false,value:undefined}
        this.show=this.show.bind(this);
        this.show1=this.show1.bind(this);
    }
    show(e)
    {
        let clickedValue=e.target.innerText;
        this.setState((prvState)=>{
            return   {show1:!prvState.show1};
        });
    }
    show1(e)
    {
        let id;
        if(e!=undefined)
            id=e.target.id;

        this.setState((prev)=>{
            if(id==prev.value)
            return {show:!prev.show,value:prev.value}
            return {show:true,value:id};
        })
    }
    render()
    {
        const data=this.props.data;
        const element=data.map((record,i)=>(<div key={i}>
            <li><Teams data={record} id={i} parentState={this.state} handler={this.show1}/></li>
        </div>));
        return (
            <div className="main">
            <h4 onClick={this.show} className="home">Home</h4>
            <ul key={0} className="list" >
                {this.state.show1 && element}
            </ul>
            </div>
        )
    }
}

export default Years;