import React,{Component} from 'react';

class Charts extends Component
{
    constructor(props)
    {
        super(props);
        this.stateValue=this.props.value;
    }
    componentWillUnmount(){
        this.props.handler()
    }
    render()
    {
        let data=this.props.data;
        let element=data.storyList.map((record,i)=>{
            return <li key={i} className="charts">{record}</li>;
        });
        return(
            <div>
                <h4 onClick={this.props.handler} id={this.props.id} className="teams">{data.team}</h4>
            <ul>
                {(this.props.parentState.value==this.props.id)&&(this.props.parentState.show) && element}
            </ul>
            </div>
        )
    }
}

export default Charts;