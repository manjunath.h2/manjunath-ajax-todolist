import React,{Component} from 'react';
import Charts from './charts';
class Teams extends Component
{
    constructor(props)
    {
        super(props);
        this.state={show:false,value:undefined}
        this.show1=this.show1.bind(this);
    }
    componentWillUnmount(){
        this.props.handler()
    }
    show1(e)
    {
        let id;
        if(e!=undefined)
            id=e.target.id;

        this.setState((prev)=>{
            if(id==prev.value)
            return {show:!prev.show,value:prev.value}
            return {show:true,value:id};
        })
    }
    render()
    {
        debugger;
        let data=this.props.data;
        let element=data.teamList.map((record,i)=>
              (<div key={i}>
            <li id={i}><Charts id={i} handler={this.show1} parentState={this.state} data={record}/></li>
        </div>)
);
        return(
            <div>
                <h4 id={this.props.id} onClick={this.props.handler} className="year">{data.year}</h4>
            <ul>
                {this.props.id==this.props.parentState.value &&(this.props.parentState.show)&& element}
            </ul>
            </div>
        )
    }
}
export default Teams;