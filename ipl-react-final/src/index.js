import React from 'react';
import ReactDom from 'react-dom';
import Years from './years';
import  './index.css';

const dataArray = [{ 
    year : 2018 ,
    teamList : [{
        team :"CSK",
        storyList :[
            "Second",
            "Three"
        ]},
        {
        team : "RCB",
        storyList :[
            "First",
            "Second",
            "Three",
            "Four"
        ]}
    ]
 },
 {
    year : 2017,
    teamList : [{
        team : "MI",
        storyList:[
            "First",
            "Second",
            "Three"
        ]
    }],
    
 }];


ReactDom.render(<Years data={dataArray}/>,document.getElementById("root"));