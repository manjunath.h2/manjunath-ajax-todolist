[
    {
    "_id": 2008,
    "teams": [
    "Kings XI Punjab",
    "Deccan Chargers",
    "Chennai Super Kings",
    "Royal Challengers Bangalore",
    "Mumbai Indians",
    "Kolkata Knight Riders",
    "Rajasthan Royals"
    ]
    },
    {
    "_id": 2009,
    "teams": [
    "Rajasthan Royals",
    "Deccan Chargers",
    "Chennai Super Kings",
    "Delhi Daredevils",
    "Kings XI Punjab",
    "Mumbai Indians",
    "Kolkata Knight Riders",
    "Royal Challengers Bangalore"
    ]
    },
    {
    "_id": 2010,
    "teams": [
    "Chennai Super Kings",
    "Deccan Chargers",
    "Rajasthan Royals",
    "Delhi Daredevils",
    "Kings XI Punjab",
    "Mumbai Indians",
    "Kolkata Knight Riders",
    "Royal Challengers Bangalore"
    ]
    },
    {
    "_id": 2011,
    "teams": [
    "Pune Warriors",
    "Rajasthan Royals",
    "Kochi Tuskers Kerala",
    "Mumbai Indians",
    "Royal Challengers Bangalore",
    "Kolkata Knight Riders",
    "Chennai Super Kings",
    "Deccan Chargers",
    "Kings XI Punjab",
    "Delhi Daredevils"
    ]
    },
    {
    "_id": 2012,
    "teams": [
    "Delhi Daredevils",
    "Kings XI Punjab",
    "Deccan Chargers",
    "Rajasthan Royals",
    "Chennai Super Kings",
    "Pune Warriors",
    "Mumbai Indians",
    "Kolkata Knight Riders",
    "Royal Challengers Bangalore"
    ]
    },
    {
    "_id": 2013,
    "teams": [
    "Chennai Super Kings",
    "Rajasthan Royals",
    "Kings XI Punjab",
    "Delhi Daredevils",
    "Sunrisers Hyderabad",
    "Kolkata Knight Riders",
    "Mumbai Indians",
    "Royal Challengers Bangalore",
    "Pune Warriors"
    ]
    },
    {
    "_id": 2014,
    "teams": [
    "Rajasthan Royals",
    "Royal Challengers Bangalore",
    "Mumbai Indians",
    "Kolkata Knight Riders",
    "Chennai Super Kings",
    "Kings XI Punjab",
    "Delhi Daredevils",
    "Sunrisers Hyderabad"
    ]
    },
    {
    "_id": 2015,
    "teams": [
    "Sunrisers Hyderabad",
    "Kings XI Punjab",
    "Delhi Daredevils",
    "Chennai Super Kings",
    "Rajasthan Royals",
    "Royal Challengers Bangalore",
    "Mumbai Indians",
    "Kolkata Knight Riders"
    ]
    },
    {
    "_id": 2016,
    "teams": [
    "Rising Pune Supergiants",
    "Gujarat Lions",
    "Sunrisers Hyderabad",
    "Delhi Daredevils",
    "Kings XI Punjab",
    "Royal Challengers Bangalore",
    "Mumbai Indians",
    "Kolkata Knight Riders"
    ]
    },
    {
    "_id": 2017,
    "teams": [
    "Kolkata Knight Riders",
    "Royal Challengers Bangalore",
    "Mumbai Indians",
    "Kings XI Punjab",
    "Delhi Daredevils",
    "Sunrisers Hyderabad",
    "Gujarat Lions",
    "Rising Pune Supergiant"
    ]
    }
    ]