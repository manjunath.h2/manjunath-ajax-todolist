// 'use strict';
man = 20; //gives error bcs man is not declared when we use strict fuction
var array = [];
var task = {};
var id = 0;
var arr = JSON.parse(localStorage.getItem("toDoList"));
if (arr != null && arr.length > 0)
    arr.forEach(element => {
        createElement(element);
    });
$(document).ready(function() {
    $("input[type=text]").keypress(function() {
        if (event.which === 13) {
            if ($(this).val() === "") {
                $(this).attr("placeholder", "Enter a task to be done");
                $(this).css("background", "red");
            } else {
                $(this).attr("placeholder", "Add more Task");
                $(this).css("background", "white");
                var text = $(this).val();
                createElement(text);
                $(this).val("");
            }
        }
    });
    $("ul").on("click", ".checkboxImage", function() {
        $($(this).siblings()[0]).toggleClass("line-on-text");
        $(this).toggleClass("background");
    });
    $("ul").on("click", ".cancelButton", function() {
        $($(this).parent()[0]).remove();
        array.splice($.inArray($(this).siblings()[1].innerText, array), 1);
        localStorage.setItem("toDoList", JSON.stringify(array));
    });
    $("ul").on("keypress", ".clickOnTask", function() {
        if (event.which === 13) {
            array[$(this).parent()[0].id] = $(this).text();
            task.name = text;
            task.check = false;
            array.push(task);
            localStorage.setItem("toDoList", JSON.stringify(array));
        }
    });
});

function createElement(text, check) {
    var listTag = ("<li class=\"ist-group-items row card\" id=" + id++ + ">" +
        ("<img class=\"col-1 checkboxImage\" src=\"/home/dev/projects/toDoList/unchecked-checkbox.png\">") +
        ("<div class=\"col-10 clickOnTask\" contenteditable=\"true\">" + text + "</div>") +
        ("<button class=\"btn-light col-1 cancelButton\">&#9747;</button>") +
        "</li>");
    $(listTag).prependTo("ul");
    array.push(text);
    localStorage.setItem("toDoList", JSON.stringify(array));
};

//need to change as dragable

//need to do more user friendly

// var pos = $(this);
// $("html").click(function(pos) {
//     alert($(pos).text());
// });